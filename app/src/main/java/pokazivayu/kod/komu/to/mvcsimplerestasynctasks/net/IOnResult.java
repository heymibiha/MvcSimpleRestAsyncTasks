package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.net;

public interface IOnResult<T> {
    void onResult(T model);

    void onError(Throwable exception);
}
