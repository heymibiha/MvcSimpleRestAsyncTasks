package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json.IJsonDeserializable;


public class Forecast implements Serializable, IJsonDeserializable {

    private static final long serialVersionUID = 1L;

    private List<Forecastday> forecastday;

    public List<Forecastday> getForecastday() {
        return forecastday;
    }

    @Override
    public void fillFromJSONObject(JSONObject jsonObject) throws JSONException, ParseException {
        if (jsonObject != null) {

            if (jsonObject.has("forecastday")) {

                JSONArray jsonArray = jsonObject.getJSONArray("forecastday");
                this.forecastday = new ArrayList(jsonArray.length());

                for (int i = 0; i < jsonArray.length(); ++i) {
                    Forecastday forecastday = new Forecastday();
                    forecastday.fillFromJSONObject(jsonArray.getJSONObject(i));
                    this.forecastday.add(forecastday);
                }
            }
        }
    }
}
