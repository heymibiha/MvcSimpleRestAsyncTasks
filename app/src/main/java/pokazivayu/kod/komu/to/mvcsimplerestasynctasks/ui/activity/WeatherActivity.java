package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.ui.activity;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.R;

public class WeatherActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_activity_layout);
    }
}
