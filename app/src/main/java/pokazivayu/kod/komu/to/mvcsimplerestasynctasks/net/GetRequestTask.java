package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.net;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.exception.NetworkException;

class GetRequestTask extends AsyncTask<String, Void, String> {
    //Hardcoded for example
    public static final String REQUEST_METHOD = "GET";
    public static final int READ_TIMEOUT = 15000;
    public static final int CONNECTION_TIMEOUT = 15000;

    private final IRequestResult mDelegate;

    public GetRequestTask(IRequestResult mDelegate) {

        this.mDelegate = mDelegate;
    }

    @Override
    protected String doInBackground(String... params) {
        String stringUrl = params[0];
        String result;
        String inputLine;
        try {

            URL myUrl = new URL(stringUrl);
            HttpURLConnection connection = (HttpURLConnection)
                    myUrl.openConnection();

            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);

            connection.connect();

            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());

            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }

            reader.close();
            streamReader.close();
            result = stringBuilder.toString();
        } catch (IOException e) {
            if (mDelegate != null) {
                mDelegate.onError(new NetworkException(e));
            }
            result = null;
        }
        return result;
    }

    protected void onPostExecute(String result) {
        if (mDelegate != null) {
            mDelegate.onResult(result);
        } else {
            super.onPostExecute(result);
        }
    }
}
