package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.exception;

public class ParseResultException extends Throwable{

    public ParseResultException(Throwable cause) {
        super(cause);
    }
}
