package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json.IJsonDeserializable;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.util.DateFormatUtil;

public class Forecastday implements Serializable, IJsonDeserializable {

    private static final long serialVersionUID = 1L;

    private Date date;

    private Day day;

    public Date getDate() {
        return date;
    }

    public Day getDay() {
        return day;
    }

    @Override
    public void fillFromJSONObject(JSONObject jsonObject) throws JSONException, ParseException {
        if (jsonObject != null) {

            if (jsonObject.has("date")) {
                this.date = DateFormatUtil.parseDateFromJson(jsonObject.getString("date"));
            }

            if (jsonObject.has("day")) {
                this.day = new Day();
                this.day.fillFromJSONObject(jsonObject.getJSONObject("day"));
            }

        }
    }
}
