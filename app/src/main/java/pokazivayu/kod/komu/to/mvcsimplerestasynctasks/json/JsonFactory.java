package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json;

import org.json.JSONException;

import java.text.ParseException;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto.WeatherModel;

public class JsonFactory {
    public static <T> T deserialize(String json, Class<T> clazz) throws JSONException, ParseException {
        if (clazz == null) {
            return null;
        }

        if (clazz == WeatherModel.class) {
            return (T) WeatherModelDeserializer.deserialize(json);
        }

        return null;
    }

    ;
}
