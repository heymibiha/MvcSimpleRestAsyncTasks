package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.net;

import org.json.JSONException;

import java.text.ParseException;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.BuildConfig;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json.JsonFactory;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto.WeatherModel;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.exception.NetworkException;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.exception.ParseResultException;

public class WeatherDao {
    public static void getKharkivWeather(final IOnResult<WeatherModel> onResult) {

        new GetRequestTask(new IRequestResult() {
            @Override
            public void onResult(String sResult) {
                try {
                    onResult.onResult(JsonFactory.deserialize(sResult, WeatherModel.class));
                } catch (JSONException e) {
                    onResult.onError(new ParseResultException(e));
                } catch (ParseException e) {
                    onResult.onError(new ParseResultException(e));
                }
            }

            @Override
            public void onError(NetworkException e) {
                onResult.onError(e);
            }
        }).execute(BuildConfig.WEATHER_URL);

    }
}
