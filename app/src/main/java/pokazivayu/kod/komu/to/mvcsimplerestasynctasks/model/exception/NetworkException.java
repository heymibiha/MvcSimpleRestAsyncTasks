package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.exception;

public class NetworkException extends Throwable{

    public NetworkException(Throwable cause) {
        super(cause);
    }
}
