package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto.WeatherModel;

class WeatherModelDeserializer {
    public static WeatherModel deserialize(String json) throws JSONException, ParseException {
        if(json ==null || json.isEmpty()) {
            return null;
        }

        WeatherModel weatherModel = new WeatherModel();
        weatherModel.fillFromJSONObject(new JSONObject(json));
        return weatherModel;
    }
}
