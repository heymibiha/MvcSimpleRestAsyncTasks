package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.net;

import java.io.IOException;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.exception.NetworkException;

interface IRequestResult {
    void onResult(String response);

    void onError(NetworkException e);
}
