package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.R;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto.WeatherModel;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.net.IOnResult;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.net.WeatherDao;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.ui.adapter.WeatherAdapter;

public class WeatherFragment extends Fragment {

    private SwipeRefreshLayout mWeatherSrl;

    private RecyclerView mWeatherRv;

    private WeatherAdapter mWeatherAdapter;

    private View mWeatherForecastHolder;

    private TextView mWeatherCityTv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weather_fragment_layout, container, false);

        this.setupUI(view);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        this.loadData();
    }

    private void setupUI(View rootView) {
        this.setupSwipeLayout(rootView);
        this.setupForecastHolder(rootView);
        this.setupTitle(rootView);
        this.setupRecyclerView(rootView);

    }

    private void setupForecastHolder(View rootView) {
        if (rootView == null) {
            return;
        }

        mWeatherForecastHolder = rootView.findViewById(R.id.weather_forecast_holder_view);
    }

    private void setupTitle(View rootView) {
        if (rootView == null) {
            return;
        }

        mWeatherCityTv = rootView.findViewById(R.id.weather_city_tv);

    }

    private void setupRecyclerView(View rootView) {
        if (rootView == null) {
            return;
        }

        mWeatherRv = rootView.findViewById(R.id.weather_recyclerview);

        mWeatherRv.setHasFixedSize(true);
        mWeatherRv.setLayoutManager(new LinearLayoutManager(this.getContext()));

        mWeatherAdapter = new WeatherAdapter();
        mWeatherRv.setAdapter(mWeatherAdapter);
    }

    private void setupSwipeLayout(View rootView) {

        if (rootView == null) {
            return;
        }

        mWeatherSrl = rootView.findViewById(R.id.weather_swiperefresh);
        mWeatherSrl.setOnRefreshListener(() -> loadData());
    }

    private void loadData() {
        mWeatherSrl.setRefreshing(true);
        mWeatherForecastHolder.setVisibility(View.GONE);

        WeatherDao.getKharkivWeather(new IOnResult<WeatherModel>() {
            @Override
            public void onResult(WeatherModel model) {
                mWeatherSrl.setRefreshing(false);
                updateData(model);
            }

            @Override
            public void onError(Throwable exception) {
                mWeatherSrl.setRefreshing(false);
                showError(exception);
            }
        });

    }

    private void updateData(WeatherModel model) {

        if (model != null) {

            mWeatherForecastHolder.setVisibility(View.VISIBLE);

            if (model.getLocation() != null) {
                mWeatherCityTv.setText(String.format(this.getString(R.string.weather_city_format), model.getLocation().getName()));
            }

            mWeatherAdapter.setData(model.getForecast() == null ? null : model.getForecast().getForecastday());
        }
    }

    private void showError(Throwable exception) {
        //Handle it somehow
        Toast.makeText(
                this.getContext(),
                exception.getCause() == null ? exception.getMessage() : exception.getCause().getMessage(),
                Toast.LENGTH_SHORT
        ).show();
    }


}
