package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json.IJsonDeserializable;

public class Location implements Serializable, IJsonDeserializable {

    private static final long serialVersionUID = 1L;

    private String name;

    public String getName() {
        return name;
    }

    @Override
    public void fillFromJSONObject(JSONObject jsonObject) throws JSONException {
        if (jsonObject != null) {
            if (jsonObject.has("name")) {
                this.name = jsonObject.getString("name");
            }
        }
    }
}
