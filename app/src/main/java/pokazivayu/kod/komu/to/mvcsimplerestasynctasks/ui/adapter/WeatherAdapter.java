package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.R;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto.Forecastday;
import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.util.DateFormatUtil;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {
    private List<Forecastday> mData;

    @NonNull
    @Override
    public WeatherAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.weather_forecast_item_layout, null);
        return new WeatherAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherAdapter.ViewHolder viewHolder, int i) {
        viewHolder.bindData(mData.get(i));
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void setData(List<Forecastday> list) {
        this.mData = list;
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mDateTv;

        private TextView mAvgTemperatureTv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mDateTv = itemView.findViewById(R.id.weather_forecast_item_date_tv);
            mAvgTemperatureTv = itemView.findViewById(R.id.weather_forecast_item_avg_temperature_tv);
        }

        public void bindData(Forecastday forecastday) {
            mDateTv.setText(DateFormatUtil.formatDate(forecastday.getDate()));

            if (forecastday.getDay() != null) {
                mAvgTemperatureTv.setText(String.valueOf(forecastday.getDay().getAvgtemp()));
            }
        }
    }
}
