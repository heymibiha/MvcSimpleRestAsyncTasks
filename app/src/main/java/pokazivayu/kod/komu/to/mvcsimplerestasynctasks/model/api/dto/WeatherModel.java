package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json.IJsonDeserializable;

public class WeatherModel implements Serializable, IJsonDeserializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Location location;

    private Forecast forecast;

    public Location getLocation() {
        return location;
    }

    public Forecast getForecast() {
        return forecast;
    }

    @Override
    public void fillFromJSONObject(JSONObject jsonObject) throws JSONException, ParseException {
        if (jsonObject != null) {

            if (jsonObject.has("location")) {
                this.location = new Location();
                this.location.fillFromJSONObject(jsonObject.getJSONObject("location"));
            }

            if (jsonObject.has("forecast")) {
                this.forecast = new Forecast();
                this.forecast.fillFromJSONObject(jsonObject.getJSONObject("forecast"));
            }

        }
    }
}
