package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.model.api.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;

import pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json.IJsonDeserializable;

public class Day implements Serializable, IJsonDeserializable {

    private static final long serialVersionUID = 1L;

    private double avgtemp;

    public double getAvgtemp() {
        return avgtemp;
    }

    @Override
    public void fillFromJSONObject(JSONObject jsonObject) throws JSONException, ParseException {
        if (jsonObject != null) {

            if (jsonObject.has("avgtemp_c")) {
                this.avgtemp = jsonObject.getDouble("avgtemp_c");
            }
        }
    }
}
