package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

public interface IJsonDeserializable {
    void fillFromJSONObject(JSONObject jsonObject) throws JSONException, ParseException;
}
