package pokazivayu.kod.komu.to.mvcsimplerestasynctasks.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatUtil {

    public static Date parseDateFromJson(String jsonValue) throws ParseException {
        if (jsonValue == null || jsonValue.isEmpty()) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.parse(jsonValue);
    }

    public static String formatDate(Date date) {
        if (date == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(date);
    }
}
